package com.sl.ms.work;

import cn.hutool.core.collection.ListUtil;
import com.sl.ms.work.domain.dto.CourierTaskCountDTO;
import com.sl.ms.work.domain.dto.PickupDispatchTaskDTO;
import com.sl.ms.work.domain.dto.request.PickupDispatchTaskPageQueryDTO;
import com.sl.ms.work.domain.enums.pickupDispatchtask.PickupDispatchTaskType;
import com.sl.ms.work.domain.enums.pickupDispatchtask.SignRecipientEnum;
import com.sl.ms.work.domain.enums.pickupDispatchtask.PickupDispatchTaskStatus;

import java.time.LocalDateTime;
import java.util.List;

import com.sl.ms.work.domain.enums.pickupDispatchtask.PickupDispatchTaskIsDeleted;
import com.sl.ms.work.domain.enums.pickupDispatchtask.PickupDispatchTaskSignStatus;
import com.sl.ms.work.domain.enums.pickupDispatchtask.PickupDispatchTaskAssignedStatus;
import com.sl.ms.work.domain.enums.pickupDispatchtask.PickupDispatchTaskCancelReason;

import com.sl.ms.work.entity.PickupDispatchTaskEntity;
import com.sl.ms.work.service.PickupDispatchTaskService;
import com.sl.transport.common.util.PageResponse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class PickupDispatchTaskTest {

    @Resource
    private PickupDispatchTaskService pickupDispatchTaskService;

    @Test
    void testSaveTask() {
        PickupDispatchTaskEntity entity = new PickupDispatchTaskEntity();
        entity.setCourierId(1024985481462545409L);
        entity.setOrderId(666L);
        entity.setAgencyId(0L);
        entity.setTaskType(PickupDispatchTaskType.PICKUP);
        entity.setMark("带包装");
        entity.setSignStatus(PickupDispatchTaskSignStatus.NOT_SIGNED);
        entity.setAssignedStatus(PickupDispatchTaskAssignedStatus.DISTRIBUTED);
        pickupDispatchTaskService.saveTaskPickupDispatch(entity);
    }

    @Test
    void testSaveBatch() {
        for (int i = 1; i <= 3; i++) {
            PickupDispatchTaskEntity entity = new PickupDispatchTaskEntity();
            entity.setCourierId(1024985344870841121L);
            long orderId = 669 + i;
            entity.setOrderId(orderId);
            entity.setAgencyId(0L);
            entity.setTaskType(PickupDispatchTaskType.PICKUP);
            entity.setMark("刘薇带包装" + i);
            entity.setSignStatus(PickupDispatchTaskSignStatus.NOT_SIGNED);
            entity.setAssignedStatus(PickupDispatchTaskAssignedStatus.DISTRIBUTED);
            pickupDispatchTaskService.saveTaskPickupDispatch(entity);
        }
    }

    @Test
    void testFindByPage() {
        PickupDispatchTaskPageQueryDTO dto = new PickupDispatchTaskPageQueryDTO();
        dto.setPage(1);
        dto.setPageSize(2);
        dto.setOrderId(666L);
        PageResponse<PickupDispatchTaskDTO> page = pickupDispatchTaskService.findByPage(dto);
        System.out.println(page);
    }

    @Test
    void testFindCountByCourierIds() {
        List<CourierTaskCountDTO> countByCourierIds = pickupDispatchTaskService.findCountByCourierIds(ListUtil.toList(1024985481462545409L, 1024985344870841121L),
                PickupDispatchTaskType.PICKUP,
                "2023-10-25");
        countByCourierIds.forEach(System.out::println);
    }

    @Test
    void testDeleteByIds() {
        List<Long> ids = ListUtil.toList(1717000534884294657L, 1717018361762607106L);
        boolean result = pickupDispatchTaskService.deleteByIds(ids);
        System.out.println("删除是否成功: " + result);
    }

    @Test
    void testUpdateCourierId() {
        Boolean result = pickupDispatchTaskService.updateCourierId(ListUtil.toList(1717019129706160130L),
                1024985344870841121L,
                1024985481462545409L);
        System.out.println("更派快递员是否成功: " + result);
    }

    @Test
    void testUpdateStatus() {
        PickupDispatchTaskDTO dto = new PickupDispatchTaskDTO();
        dto.setId(1717018361762607106L);
        dto.setStatus(PickupDispatchTaskStatus.COMPLETED);
        Boolean result = pickupDispatchTaskService.updateStatus(dto);
        System.out.println("更新状态是否成功: " + result);
    }
}
