package com.sl.mongodb.service.impl;

import com.sl.mongodb.entity.Person;
import com.sl.mongodb.service.PersonService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Resource
    private MongoTemplate mongoTemplate;

    /**
     * 新增数据
     *
     * @param person 数据
     */
    @Override
    public void savePerson(Person person) {
        mongoTemplate.save(person);
    }

    /**
     * 批量保存数据
     *
     * @param personList 用户列表
     */
    @Override
    public void savePersonBatch(List<Person> personList) {
        mongoTemplate.insertAll(personList);
    }

    /**
     * 更新数据
     *
     * @param person 数据
     */
    @Override
    public void update(Person person) {
        // 1. 查询条件
        Query query = Query.query(Criteria.where("id").is(person.getId()));
        // 2. 设置修改对象
        Update update = new Update();
        update.set("age", person.getAge());
        update.set("name", person.getName());
        // 3. 执行修改方法
        mongoTemplate.updateFirst(query, update, Person.class);
    }

    /**
     * 根据名字查询用户列表
     *
     * @param name 用户名字
     * @return 用户列表
     */
    @Override
    public List<Person> queryPersonListByNameAndAge(String name, Integer age) {
        Query query = Query.query(Criteria.where("name").regex(".*" + name + ".*").and("age").lt(age));
        List<Person> personList = mongoTemplate.find(query, Person.class);
        return personList;
    }

    /**
     * 分页查询用户列表
     *
     * @param page     页数
     * @param pageSize 页面大小
     * @return 用户列表
     */
    @Override
    public List<Person> queryPersonPageList(int page, int pageSize) {
        Query query = Query.query(Criteria.where("name").regex(".*" + "王五" + ".*"));
        query.with(PageRequest.of(page - 1, pageSize));
        query.with(Sort.by(Sort.Direction.DESC, "age"));
        return mongoTemplate.find(query, Person.class);
    }

    /**
     * 根据id删除用户
     *
     * @param id 主键
     */
    @Override
    public void deleteById(String id) {
        mongoTemplate.remove(Query.query(Criteria.where("_id").is(id)), Person.class);
    }

}
