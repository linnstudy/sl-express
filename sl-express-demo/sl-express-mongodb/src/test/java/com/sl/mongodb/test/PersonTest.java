package com.sl.mongodb.test;

import com.sl.mongodb.entity.Address;
import com.sl.mongodb.entity.Person;
import com.sl.mongodb.service.PersonService;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SpringBootTest
public class PersonTest {

    @Resource
    private PersonService personService;

    @Test
    public void testSave() {
        Person person = Person.builder()
                .id(ObjectId.get())
                .name("张三")
                .age(20)
                .location(new GeoJsonPoint(116.343847, 40.060539))
                .address(new Address("人民路", "上海市", "666666")).build();
        personService.savePerson(person);
    }

    @Test
    public void testUpdate() {
        Person person = Person.builder()
                .id(new ObjectId("65322e172ce35e302f56fd47"))
                .name("李四")
                .age(30)
                .build();
        personService.update(person);
    }

    @Test
    public void testSaveBatch() {
        List<Person> personList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            Person person = Person.builder()
                    .id(ObjectId.get())
                    .name("王五" + i)
                    .age(new Random().nextInt(100))
                    .location(new GeoJsonPoint(116.343847, 40.060539))
                    .address(new Address("人民路", "上海市", "666666")).build();
            personList.add(person);
        }
        personService.savePersonBatch(personList);
    }

    @Test
    public void testQueryPersonListByNameAndAge() {
        List<Person> personList = personService.queryPersonListByNameAndAge("王五", 50);
        personList.forEach(item -> {
            System.out.println(item.getName() + "-" + item.getAge());
        });
    }

    @Test
    public void queryPersonPageList() {
        List<Person> personList = personService.queryPersonPageList(1,5);
        personList.forEach(item -> {
            System.out.println(item.getName() + " " + item.getAge());
        });
    }

    @Test
    public void testDeleteById() {
        personService.deleteById("65322e172ce35e302f56fd47");
    }

}
