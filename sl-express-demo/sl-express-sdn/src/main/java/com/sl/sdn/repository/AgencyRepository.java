package com.sl.sdn.repository;

import com.sl.sdn.entity.node.AgencyEntity;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface AgencyRepository extends Neo4jRepository<AgencyEntity, Long> {

    /**
     * 根据bid查询
     */
    AgencyEntity findByBid(Long bid);

    /**
     * 根据bid删除
     */
    void deleteByBid(Long bid);

}
