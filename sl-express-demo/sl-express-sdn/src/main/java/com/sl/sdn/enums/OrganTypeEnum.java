package com.sl.sdn.enums;

import cn.hutool.core.util.EnumUtil;
import com.sl.transport.common.enums.BaseEnum;
import io.swagger.models.auth.In;

public enum OrganTypeEnum implements BaseEnum {

    OLT(1, "一级转运中心"),

    TLT(2, "二级转运中心"),

    AGENCY(3, "网点");

    private final Integer code;

    private final String value;

    OrganTypeEnum(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static OrganTypeEnum codeOf(Integer code) {
        return EnumUtil.getBy(OrganTypeEnum::getCode, code);
    }

}
