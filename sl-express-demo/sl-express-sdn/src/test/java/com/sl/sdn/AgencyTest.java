package com.sl.sdn;

import cn.hutool.core.bean.BeanUtil;
import com.sl.sdn.entity.node.AgencyEntity;
import com.sl.sdn.repository.AgencyRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class AgencyTest {

    @Resource
    private AgencyRepository agencyRepository;

    @Test
    public void testSave() {
        AgencyEntity agencyEntity = new AgencyEntity();
        agencyEntity.setAddress("测试数据地址");
        agencyEntity.setBid(9001L);
        agencyEntity.setName("测试节点");
        agencyEntity.setPhone("13800138000");
        agencyRepository.save(agencyEntity);
    }

    @Test
    public void testFindByBid() {
        AgencyEntity agencyEntity = agencyRepository.findByBid(9001L);
        System.out.println(agencyEntity.getName() + " " + agencyEntity.getAddress());
    }

    @Test
    public void testUpdate() {
        AgencyEntity agencyEntity = agencyRepository.findByBid(9001L);
        System.out.println("更新前的名称: " + agencyEntity.getName());
        agencyEntity.setName("测试节点666");
        agencyRepository.save(agencyEntity);
        agencyEntity = agencyRepository.findByBid(9001L);
        System.out.println("更新后的名称: " + agencyEntity.getName());
    }

    @Test
    public void testDeleteByBid() {
        Long bid = 9001L;
        agencyRepository.deleteByBid(bid);
        AgencyEntity agencyEntity = agencyRepository.findByBid(9001L);
        if (BeanUtil.isEmpty(agencyEntity)) {
            System.out.println(bid + "数据删除成功");
        } else {
            System.out.println("删除失败");
        }
    }

}
