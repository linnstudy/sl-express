package com.sl.sdn;

import cn.hutool.json.JSONUtil;
import com.sl.sdn.entity.dto.TransportLineNodeDTO;
import com.sl.sdn.entity.node.AgencyEntity;
import com.sl.sdn.repository.TransportLineRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class TransportLineTest {

    @Resource
    private TransportLineRepository transportLineRepository;

    @Test
    public void testFindShortestPath() {
        AgencyEntity start = AgencyEntity.builder().bid(100280L).build();
        AgencyEntity end = AgencyEntity.builder().bid(210057L).build();
        TransportLineNodeDTO shortestPath = transportLineRepository.findShortestPath(start, end);
        System.out.println(JSONUtil.toJsonStr(shortestPath));
    }

    @Test
    public void testFindMinCostPath() {
        AgencyEntity start = AgencyEntity.builder().bid(100280L).build();
        AgencyEntity end = AgencyEntity.builder().bid(210057L).build();
        TransportLineNodeDTO minCostPath = transportLineRepository.findMinCostPath(start, end);
        System.out.println(JSONUtil.toJsonStr(minCostPath));
    }

}
