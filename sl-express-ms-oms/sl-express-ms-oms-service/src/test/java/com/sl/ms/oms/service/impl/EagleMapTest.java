package com.sl.ms.oms.service.impl;

import com.itheima.em.sdk.EagleMapTemplate;
import com.itheima.em.sdk.enums.ProviderEnum;
import com.itheima.em.sdk.vo.Coordinate;
import com.itheima.em.sdk.vo.GeoResult;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class EagleMapTest {

    @Resource
    private EagleMapTemplate eagleMapTemplate;

    //根据位置名称获取经纬度
    @Test
    public void getGeoPoint() {
        GeoResult geoResult = eagleMapTemplate.opsForBase().geoCode(ProviderEnum.AMAP, "广州塔", null);
        System.out.println(geoResult);
    }

    //根据经纬度获取位置名称
    @Test
    public void getAddress() {
        GeoResult geoResult = eagleMapTemplate.opsForBase().geoDecode(ProviderEnum.AMAP, 113.457185, 23.259042, null);
        System.out.println(geoResult);
    }

    //获取两点之间的路线规划
    @Test
    public void getLine() {
        Coordinate origin = new Coordinate(113.457185, 23.259042);
        Coordinate destination = new Coordinate(113.324553, 23.106414);
        String result = eagleMapTemplate.opsForDirection().driving(ProviderEnum.AMAP, origin, destination);
        System.out.println(result);
    }

}
