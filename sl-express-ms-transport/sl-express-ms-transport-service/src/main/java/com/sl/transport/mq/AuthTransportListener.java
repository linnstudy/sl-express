package com.sl.transport.mq;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.sl.transport.common.constant.Constants;
import com.sl.transport.entity.node.AgencyEntity;
import com.sl.transport.entity.node.BaseEntity;
import com.sl.transport.entity.node.OLTEntity;
import com.sl.transport.entity.node.TLTEntity;
import com.sl.transport.enums.OrganTypeEnum;
import com.sl.transport.service.IService;
import com.sl.transport.utils.OrganServiceFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AuthTransportListener {

    @RabbitListener(bindings = @QueueBinding(value = @Queue(Constants.MQ.Queues.AUTH_TRANSPORT),
            exchange = @Exchange(value = "${rabbitmq.exchange}",
                    type = ExchangeTypes.TOPIC), key = "#"))
    public void handleAuthTransport(String msg) {
        log.info("接收到权限管家机构变更消息: ==> {}", msg);
        //将msg转为JSON对象
        JSONObject jsonObject = JSONUtil.parseObj(msg);
        //获取type，如果不为ORG直接return
        String type = jsonObject.getStr("type");
        if (!StrUtil.equalsIgnoreCase(type, "ORG")) {
            log.warn("接收到权限非机构消息，无需处理: ==> {}", msg);
            return;
        }
        //获取operation
        String operation = jsonObject.getStr("operation");
        //获取content 约定第0个中含有name
        JSONObject content = (JSONObject) jsonObject.getJSONArray("content").get(0);
        String name = content.getStr("name");
        Long parentId = content.getLong("parentId");
        //定义IService BaseEntity
        IService iService;
        BaseEntity baseEntity;
        //判断name以什么结尾 初始化IService、BaseEntity
        if (StrUtil.endWith(name, "转运中心")) {
            iService = OrganServiceFactory.getBean(OrganTypeEnum.OLT.getCode());
            baseEntity = new OLTEntity();
        } else if (StrUtil.endWith(name, "分拣中心")) {
            iService = OrganServiceFactory.getBean(OrganTypeEnum.TLT.getCode());
            baseEntity = new TLTEntity();
        } else if (StrUtil.endWith(name, "营业部")) {
            iService = OrganServiceFactory.getBean(OrganTypeEnum.AGENCY.getCode());
            baseEntity = new AgencyEntity();
        } else {
            return;
        }
        //补全实体参数 parentId bid name status
        baseEntity.setParentId(parentId);
        baseEntity.setBid(content.getLong("id"));
        baseEntity.setName(name);
        baseEntity.setStatus(content.getBool("status"));
        //不同的操作类型，调用IService的不同方法实现
        switch (operation) {
            case "ADD":
                iService.create(baseEntity);
                break;
            case "UPDATE":
                iService.update(baseEntity);
                break;
            case "DEL":
                iService.deleteByBid(baseEntity.getBid());
                break;
        }
    }

}
