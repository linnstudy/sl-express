package com.sl.transport;

import com.sl.transport.domain.OrganDTO;
import com.sl.transport.repository.OrganRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
public class OrganRepositoryTest {
    @Resource
    private OrganRepository organRepository;

    @Test
    public void testFindByBid() {
        OrganDTO organDTO = organRepository.findByBid(25023L);
        System.out.println(organDTO);
    }

    @Test
    public void testFindByBids() {
        List<OrganDTO> list = organRepository.findByBids(List.of(25023L));
        for (OrganDTO organDTO : list) {
            System.out.println(organDTO.getName());
        }
    }

    @Test
    public void testFindAll() {
        List<OrganDTO> organDTOList = organRepository.findAll("北京");
        for (OrganDTO organDTO : organDTOList) {
            System.out.println(organDTO.getName());
        }
    }
}
