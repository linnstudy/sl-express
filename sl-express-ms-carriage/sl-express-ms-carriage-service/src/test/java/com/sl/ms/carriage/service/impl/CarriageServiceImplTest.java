package com.sl.ms.carriage.service.impl;

import com.sl.ms.carriage.domain.dto.CarriageDTO;
import com.sl.ms.carriage.domain.dto.WaybillDTO;
import com.sl.ms.carriage.service.CarriageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CarriageServiceImplTest {

    @Resource
    private CarriageService carriageService;

    @Test
    void saveOrUpdate() {
        CarriageDTO carriageDTO = new CarriageDTO();
        carriageDTO.setTemplateType(3);
        carriageDTO.setTransportType(1);
        carriageDTO.setAssociatedCityList(Arrays.asList("5"));
        carriageDTO.setFirstWeight(12d);
        carriageDTO.setContinuousWeight(1d);
        carriageDTO.setLightThrowingCoefficient(6000);
        CarriageDTO dto = carriageService.saveOrUpdate(carriageDTO);
        System.out.println(dto);
    }

    @Test
    void findAll() {
        List<CarriageDTO> carriageDTOList = carriageService.findAll();
        System.out.println(carriageDTOList);
    }

    @Test
    void compute() {
        WaybillDTO waybillDTO = new WaybillDTO();
        waybillDTO.setReceiverCityId(7363L);//天津
        waybillDTO.setSenderCityId(2L);//北京
        waybillDTO.setWeight(3.8);//重量
        waybillDTO.setVolume(125000);//体积
        CarriageDTO compute = this.carriageService.compute(waybillDTO);
        System.out.println("重量: " + compute.getComputeWeight() + ", 价格: " + compute.getExpense());
    }

}